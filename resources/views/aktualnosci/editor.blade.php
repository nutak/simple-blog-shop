@extends('base')

@section('content')

<?php
    //var_dump($news);
?>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
  
    <h3>Aktualności - edytor</h3>
    
    <form method="post">
        @if($news)
            <input type="hidden" value="{{ $news[0]->id }}" name="newsid"/>
        @endif   
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="text" value="@if($news) {{ $news[0]->title }} @endif" placeholder="Tytuł wpisu" class="form-control" name="title"/><br>
        <textarea name="body" placeholder="Treść wpisu">
            @if($news)
                <?=$news[0]->body ?>
            @endif            
        </textarea><br>
        <input type="submit" class="btn btn-success" value="Prześlij"><br>
    </form>
    
@endsection