<?php
        //var_dump($newsy);
    
       foreach($newsy as $news)
       {
           if(!Auth::guest() &&  Auth::user()->roleid!=2 && $news->active!=1)
               continue;
           
          $counter++;
    ?>
        <div id="n<?=$counter?>" class="row news_entry">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default" >
                            <!--div class="panel-heading" style="float:left; width:100%;"><h4 style="float:left">{{ $news->title }}</h4><div style="float:right; line-height: 40px; vertical-align: middle;">Utworzono: {{ $news->created_at }}</div></div>-->
                            <div class="panel-heading"><h4>{{ $news->title }}</h4></div>
				<div class="panel-body">
                                    <p><small>Utworzono: {{ $news->created_at }}, przez: {{ $news->name }}</small>
                                    @if (!Auth::guest() && Auth::user()->roleid==2)
                                    <a class="glyphicon glyphicon-pencil" href="{{ url() }}/aktualnosci/wpis?newsid={{ $news->id }}"></a>
                                    <a class="glyphicon glyphicon-remove" href="{{ url() }}/aktualnosci/wpis?newsid={{ $news->id }}"></a>
                                    @endif
                                    </p>
					<?=$news->body?>
				</div>
			</div>
		</div>
	</div>
    <?php
        
       } 
    ?>