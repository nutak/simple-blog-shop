@extends('base')

@section('content')

<?php
    //var_dump($news);
?>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
  
    <h3>Kategoria - edytor</h3>
    
    <form method="post">
        @if($cat)
            <input type="hidden" value="{{ $cat[0]->id }}" name="newsid"/>
        @endif   
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="text" value="@if($cat) {{ $cat[0]->title }} @endif" placeholder="Tytuł" class="form-control" name="title"/><br>
		<?php
		/*
        <textarea name="body" placeholder="Treść wpisu">
            @if($news)
                
            @endif            
        </textarea><br>
		*/ ?>
        <input type="submit" class="btn btn-success" value="Prześlij"><br>
    </form>
    
@endsection