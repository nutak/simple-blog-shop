@extends('base')

@section('content')
<style>
    .gal_albums
    {
        width: 100%;
        height: 360px;
        padding: 10px;
        margin-bottom: 15px;
        overflow: auto;
        /* display: -webkit-box; */
        white-space: nowrap;
    }
    
    .gal_albums::-webkit-scrollbar {
         width: 12px;
     }

     .gal_albums::-webkit-scrollbar-track {
         -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
         border-radius: 10px;
     }

     .gal_albums::-webkit-scrollbar-thumb {
         border-radius: 10px;
         -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
     }
    
    .s_album
    {
        width:  280px;
        height: 280px;
        margin:10px;
        float:left;
        overflow: hidden;
        
        position: relative;
        display: block;
    }
    
    .s_album>.cegla
    {
        overflow: hidden;
        font-size:                  1.33em;
        line-height:                1;
        height:                     2em;
    }
    
    .s_album>.cegla:after
    {
        content:                    ' ';
        position:                   absolute;
        display:                    block;
        width:                      100%;
        height:                     1em;
        
        bottom:                     0px;
        left:                       0px;
    }
    
    .s_album>.cegla:before
    {
        content:                    ' ';
        text-align:                 right;
        position:                   absolute;
        display:                    block;
        width:                      2em;
        height:                     1em;
        bottom:                     1em;
        right:                      20px;
    }
    
    .discreetlink
    {
        text-decoration: none;
        color:black;
        display: inline-block;
        margin: 5px;
        white-space: normal;
    }
    .discreetlink:hover
    {
        text-decoration: none;
        color:#444;
        
    }
    
    #lightbox-image
    {
        max-height:500px;
    }
</style>

    <h3>Galeria</h3>
   <?php
    
    foreach($gallery as $categories)
    {
        ?>
    <div class="row gal_cat">
        <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-lg-10 col-md-10 col-sm-10 col-xs-12">
            <h4><?=$categories->title?></h4>
            @if(!empty($categories->album))
            <div class="gal_albums">
                <?php
                foreach($categories->album as $album)
                {
                    ?>
                    <a class="discreetlink" onclick="loadAlbum(<?=$album->id?>)">
                        <div class="well s_album">
								
                            <?=($album->miniature_url=='' || $album->miniature_url==NULL ? '<span title="brak miniatury" style="font-size:40px; line-height:200px; text-align:center; width:100%;" class="glyphicon glyphicon-eye-close"></span>' : '<img src="' . $album->miniature_url . '" style="height:200px;">')?>
                            <div class="cegla"><?=$album->title?></div>
							@if (!Auth::guest() && Auth::user()->roleid==2)
                                    <a class="glyphicon glyphicon-pencil" href="{{ url() }}/galeria/album?albumid={{ $album->id }}"></a>
                                    <a class="glyphicon glyphicon-remove" href="{{ url() }}/galeria/album_rm?albumid={{ $album->id }}"></a>
                                @endif
                        </div>
                    </a>
                    <?php
                }
                ?>
            </div>
            @endif
        </div>
    </div>
    
        <?php
    }
   ?>
    

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="css/bootstrap-image-gallery.css">

    
    <div class="photos_to_show" style="">
        
    </div>
    
    
   
@endsection