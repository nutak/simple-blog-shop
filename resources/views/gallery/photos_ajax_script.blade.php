@extends('base')

@section('script')

<script src="{{ url('/js/jquery.lightbox-0.5.min.js') }}"></script>
<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="{{ url('/js/bootstrap-image-gallery.js') }}"></script>
<!--script src="{{ url('/js/demo.js') }}"></script>-->

<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        
                    </button>
                    <button type="button" class="btn btn-default next">
                        
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
        function loadAlbum(albumid)
        {
            //ajax
            $.ajax(
            {
                method: "GET",
                url: "/gallery/ajax/photos",
                data: { albumid: albumid },
                async: true
            }).done(function( msg )
            {
                  $('.photos_to_show').empty();
                  
                  $('.photos_to_show').append(msg);
                  $('.photos_to_show a').trigger('click');
                  //$('.photos_to_show a').lightBox();
            });

        }
    </script>

@endsection