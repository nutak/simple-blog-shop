@extends('base')

@section('content')

<?php
    //var_dump($news);
?>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
  
    <h3>Album - edytor</h3>
    
    <form method="post">
        @if($album)
            <input type="hidden" value="{{ $album[0]->id }}" name="newsid"/>
        @endif   
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="text" value="@if($album) {{ $album[0]->title }} @endif" placeholder="Tytuł" class="form-control" name="title"/><br>
        <textarea name="body" placeholder="Treść">
            @if($album)
                <?=$album[0]->body ?>
            @endif            
        </textarea><br>
        <input type="submit" class="btn btn-success" value="Prześlij"><br>
    </form>
    
@endsection