<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/base.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body style="background-image: url('bg1.jpg'); background-image: url('../bg1.jpg');">
    
    <video autoplay loop poster="{{ asset('/bg1.jpg') }}" id="bgvid">
        <source src="{{ asset('/bg2.webm') }}" type="video/webm">
        <source src="{{ asset('/bg2.mp4') }}" type="video/mp4">
    </video>
<!--HEAD-->
<div class="container" style="background-color: rgba(0,0,0,0.35);">
<div class="logo">
</div>
<nav class="navbar navbar-default"
	style="    
	border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    border: 0px solid;
    margin: 0;
    background-color: transparent;
    color: white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?=url()?>">
        <img alt="LogoMini" src="...">
      </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li><a href="<?=url()?>/aktualnosci/">Aktualności</a></li>
            <li><a href="<?=url()?>/uslugi/">Usługi</a></li>
            <li><a href="<?=url()?>/galeria/">Galeria</a></li>
            <li><a href="<?=url()?>/kontakt/">Kontakt</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            @if (Auth::guest())
                <!--li><a href="{{ url('/auth/login') }}">Logowanie</a></li>
                <li><a href="{{ url('/auth/register') }}">Register</a></li-->
            @else
                <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/auth/logout') }}">Wyloguj</a></li>
                        </ul>
                </li>
            @endif
        </ul>
    </div>
  </div>
</nav>
</div>
<!--END-HEAD-->	
<div class="container" style="background-color: rgba(255,255,255,0.85); padding-bottom:15px; margin-bottom:15px; padding-top: 5px;">
@yield('content')
</div>
<!--FOOT-->
<div class="container" style="background-color: rgba(0,0,0,0.35); color:white;">
	<div style="height:80px; line-height: 80px; text-align:center; color:white">
            <b>BETONOWE PRODUKCJE</b>
	</div>
</div>
<!--END-FOOT-->	
	<!--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
        <script src="<?=url('/js/jquery-1.10.1.min.js')?>"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
        
        @yield('script')
        
</body>
</html>
