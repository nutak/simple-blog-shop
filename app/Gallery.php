<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
// instance of Posts class will refer to posts table in database
class Gallery extends Model {
  //restricts columns from modifying
  protected $guarded = [];
  // posts has many comments
  // returns all comments on that post
  public function GetCategories()
  {
      $results = DB::select('select * FROM tofu_gallery_categories gc' 
              . ' ORDER BY gc.order ASC ');
      
      return $results;
  }
  
  public function GetCategory($kek)
  {
      $results = DB::select('select * FROM tofu_gallery_categories gc WHERE gc.id= :id' 
              . ' ORDER BY gc.order ASC '
			  , [
                 'id'       => $kek
				]
				);
      
      return $results;
  }
  
  public function GetAlbum($kek)
  {
      $results = DB::select('select * FROM tofu_gallery_albums gc WHERE gc.id= :id' 
              . ' '
			  , [
                 'id'       => $kek
				]
				);
      
      return $results;
  }
  
  
  function EditCategoryGallery($catid, $post, $userid)
  {
	  $result = DB::update('UPDATE tofu_gallery_categories SET title= :title, updated_at = :upd WHERE id= :id'
              , [
                 'id'       => $catid
                 ,'shortname'    => $postdata['title']
                 ,'title'   => $postdata['title']
                 ,'upd'     => date('Y-m-d H:i:s')
                ]
              );
  }
  
  function AddAlbumGallery($post, $userid)
  {
	  $result = DB::insert('INSERT INTO tofu_gallery_albums(author_id, title, shortname, minitature_url, body, active, created_at, updated_at) VALUES(?,?,?,?,?,?,?,?)'
              ,array($userid, $postdata['title'], $postdata['title'], $postdata['jpg'], $postdata['body'], 1, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')));
      
      $result = DB::getPdo()->lastInsertId();
      
      return $result;
  }
  
  function RmAlbumGallery($categoryid, $userid)
  {
	  //DB::table('tofu_gallery_categories')->where('id', '=', $categoryid)->delete();
	  $result = DB::update('UPDATE tofu_gallery_albums SET active= :active, updated_at = :upd WHERE id= :id'
              , [
                 'id'       => $catid
				 ,'upd'     => date('Y-m-d H:i:s')
                 ,'active'    => $postdata['active']
                ]
              );
  }
  
  function EditAlbumGallery($catid, $postdata, $userid)
  {
	  $result = DB::update('UPDATE tofu_gallery_albums SET miniature_url= :miniature_url, body= :body, title= :title, shortname= :shortname, updated_at = :upd WHERE id= :id'
              , [
                 'id'       => $catid
                 ,'shortname'    => $postdata['title']
                 ,'title'   => $postdata['title']
                 ,'body'   => $postdata['body']
                 ,'miniature_url'   => $postdata['jpg']
                 ,'upd'     => date('Y-m-d H:i:s')
                ]
              );
  }
  
  
  function AddCategoryGallery($post, $userid)
  {
	  $result = DB::insert('INSERT INTO tofu_gallery_categories(author_id, title, shortname, active, created_at, updated_at) VALUES(?,?,?,?,?,?)'
              ,array($userid, $postdata['title'], $postdata['title'], 1, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')));
      
      $result = DB::getPdo()->lastInsertId();
      
      return $result;
  }
  
  function RmCategoryGallery($categoryid, $userid)
  {
	  //DB::table('tofu_gallery_categories')->where('id', '=', $categoryid)->delete();
	  $result = DB::update('UPDATE tofu_gallery_categories SET active= :active, updated_at = :upd WHERE id= :id'
              , [
                 'id'       => $catid
				 ,'upd'     => date('Y-m-d H:i:s')
                 ,'active'    => $postdata['active']
                ]
              );
  }
  
  public function GetAllAlbums()
  {
      $results = DB::select('select * FROM tofu_gallery_albums ga' 
              . ' ORDER BY ga.order ASC ');
      
      return $results;
  }
  
  public function GetPhotosByAlbum($albumid)
  {
      $results = DB::select('select * FROM tofu_gallery_photos gp WHERE gp.album_id=? ' 
              . ' ORDER BY gp.order ASC ', array($albumid));
      
      return $results;
  } 
  
  public function AddPhotoGallery($albumid, $postdata, $userid)
  {
		$result = DB::insert('INSERT INTO tofu_gallery_photos(author_id, album_id, title, photo_url, minitature_url, shortname, hide, created_at, updated_at) VALUES(?,?,?,?,?,?,?,?,?)'
              ,array($userid, $albumid, '', $postdata['jpg'], $postdata['jpg'],'', 0, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')));
      
      $result = DB::getPdo()->lastInsertId();
      
      return $result;
  }
  
  public function RmPhotoGallery($photoid, $userid)
  {
	  $result = DB::update('UPDATE tofu_gallery_photos SET hide= :hide, updated_at = :upd WHERE id= :id'
              , [
                 'id'       => $photoid
				 ,'upd'     => date('Y-m-d H:i:s')
                 ,'hide'    => 1
                ]
              );
  }
  
  
}


            