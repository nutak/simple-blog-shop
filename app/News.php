<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
// instance of Posts class will refer to posts table in database
class News extends Model {
  //restricts columns from modifying
  protected $guarded = [];
  // posts has many comments
  // returns all comments on that post
  public function ReadNews($limit=0)
  {
      $results = DB::select('select n.id, n.author_id, n.active,  n.title, n.body, n.created_at, n.updated_at, u.name from tofu_news n'
              . ' left join users u ON u.id = n.author_id '
              . ' left join tofu_role r ON r.id=u.roleid'
              . ' ORDER BY n.created_at DESC '
              . ' LIMIT ?, 15', array($limit));
      return $results;
  }
  
  public function InsertNews($postdata ,$userid)
  {
      $result = DB::insert('INSERT INTO tofu_news(author_id, title, body, active, created_at) VALUES(?,?,?,?,?)'
              ,array($userid, $postdata['title'], $postdata['body'], 1, date('Y-m-d H:i:s')));
      
      $result = DB::getPdo()->lastInsertId();
      
      return $result;
  }
  
  public function ReadSingleNewsDetail($newsid)
  {
      $results = DB::select('select * from tofu_news n where id = :id'              
              , ['id' => $newsid]);
      
      //var_dump($results);
      
      return $results;
  }
  
  public function EditNews($newsid, $postdata, $userid)
  {
      
      $result = DB::update('UPDATE tofu_news SET title= :title, body = :body, updated_at = :upd WHERE id= :id'
              , [
                 'id'       => $newsid
                 ,'body'    => $postdata['body']
                 ,'title'   => $postdata['title']
                 ,'upd'     => date('Y-m-d H:i:s')
                ]
              );
     //var_dump($result);
  }
  
  public function DeleteNews($newsid)
  {
      
  }
}