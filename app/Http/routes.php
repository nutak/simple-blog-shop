<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('aktualnosci', 'News@index');
Route::get('aktualnosci/nowy', 'News@new_entry');
Route::get('aktualnosci/wpis/', 'News@edit_entry');
Route::post('aktualnosci/wpis/', 'News@entry_add_edit');
Route::get('aktualnosci/ajax/news', 'News@MoreNews_ajax');

Route::get('galeria', 'Gallery@index');
Route::get('gallery/ajax/photos', 'Gallery@photos_ajax');
Route::get('galeria/album/', 'Gallery@edit_album');
Route::post('galeria/album/', 'Gallery@addedit_album');
Route::get('galeria/album_rm/', 'Gallery@rem_album');
Route::post('galeria/cat', 'Gallery@addedit_category');
Route::get('galeria/cat_rm', 'Gallery@rem_category');

Route::get('administracja', 'Admin@index');

Route::get('uslugi', 'News@index');
Route::get('kontakt', 'News@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
