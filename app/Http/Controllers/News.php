<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use Illuminate\Http\Request;

class News extends Controller {

        private $news_model;
        
        public function __construct()
        {
            $this->news_model = new \App\News();
        }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            $limit=0;
            
            $news = $this->news_model->ReadNews($limit);
           
            return view('aktualnosci/main')->with([
                'newsy' => $news
                ])->nest(1, 'aktualnosci/newsloader_ajax');
            
            //view('aktualnosci/newsloader_ajax');
            
		//
	}
        
        function MoreNews_ajax()//\App\Http\Requests\NewsIndex $request
        {
            
            $limit=Input::get('limit');
            //$limit = $request->get('limit');
            if($limit==false || $limit==0)
                $limit=0;
            
            $news = $this->news_model->ReadNews($limit);
            
            //var_dump($news);
            
            $theview = view('aktualnosci/newshtml_ajax')->with([
                'newsy' => $news,
                'counter' => $limit
                ]);
            
            echo $theview;
            
        }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function new_entry()
	{
            if(Auth::guest() || Auth::user()->roleid!=2)
                return view('aktualnosci/fucku');
                        
            return view('aktualnosci/editor')->with([
                'news' => false
                ]);
		//
	}
        
        public function entry_add_edit(\App\Http\Requests\Wpis $request)
	{
            
            if(Auth::guest() ||  Auth::user()->roleid!=2)
                return view('aktualnosci/fucku');
            
            $newsid = false;
            $news[0] = new \stdClass();
            
            //var_dump($request->get('newsid'));
            
            if ($request->get('newsid',0)!=0)
            {
                                
                $newsid             =  $request->get('newsid');
                $news[0]->title     =  $request->get('title');
                $news[0]->body      =  $request->get('body');
                $news[0]->newsid    =  $newsid;
                
                $postdata['body'] = $news[0]->body;
                $postdata['title'] = $news[0]->title;
            
                $this->news_model->EditNews($newsid, $postdata, Auth::user()->id);
            }
            else
            {
                $postdata['body'] = $request->get('body');
                $postdata['title'] = $request->get('title');
            
                $newsid = $this->news_model->InsertNews($postdata, Auth::user()->id);
            }
            
            return view('aktualnosci/editor')->with([
                'news' => (!$newsid ? false : $this->news_model->ReadSingleNewsDetail($newsid))
                ]);
            
	}

        public function edit_entry()
        {
            
            if(Auth::guest() || Auth::user()->roleid!=2)
                return view('aktualnosci/fucku');
            
            $newsid = false;
            $news = false;
            
            if (Input::has('newsid'))
            {
                $newsid = Input::get('newsid');
                $news = $this->news_model->ReadSingleNewsDetail($newsid);
            }
            
            //var_dump($news);
            
            return view('aktualnosci/editor')->with([
                'news' => $news
                ]);
			
        }
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
