<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Auth;
use Input;


class Gallery extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    
        private $gallery_model;
    
        public function __construct()
        {
            $this->gallery_model = new \App\Gallery();
        }
          
	public function index()
	{
            //load categories
            $categories = $this->gallery_model->GetCategories();
            
            //load all albums
            $albums = $this->gallery_model->GetAllAlbums();
            
            
            //var_dump($albums);
            //sort shit
            foreach($categories as $cat_key => $cat)
            {
                foreach($albums as $album_key => $album)
                {
                    if($cat->id == $album->category_id)
                    {
                        $categories[$cat_key]->album[$album_key] = $album;
                        continue;
                    }
                }
            }
            
            //WŁUALA!
            
            //return  view('gallery/photos_ajax_script');
            
            return view('gallery/main')->with([
                'gallery' => $categories
            ])->nest(1, 'gallery/photos_ajax_script');
	}
        
	public function photos_ajax()
	{
		$albumid = Input::get('albumid');
		
		if($albumid==null || $albumid<1)
		{
			echo 'false';
			return ;
		}
		
		$photos = $this->gallery_model->GetPhotosByAlbum($albumid);
		
		$theview = view('gallery/photoshtml_ajax')
				->with([
			'photos' => $photos
			]);
		
		echo $theview;
	}
	
	public function addedit_category(\App\Http\Requests\Category $request)
	{
		if(Auth::guest() ||  Auth::user()->roleid!=2)
                return $this->fuckyou();
			
		$categoryid = false;
		$news[0] = new \stdClass();
		
		//var_dump($request->get('newsid'));
		
		if ($request->get('categoryid',0)!=0)
		{
							
			$categoryid         =  $request->get('categoryid');
			$news[0]->title     =  $request->get('title');
			$news[0]->newsid    =  $categoryid;
			
			$postdata['title'] = $news[0]->title;
		
			$this->gallery_model->EditCategoryGallery($categoryid, $postdata, Auth::user()->id);
		}
		else
		{
			//$postdata['body'] = $request->get('body');
			$postdata['title'] = $request->get('title');
		
			$newsid = $this->gallery_model->AddCategoryGallery($postdata, Auth::user()->id);
		}
		
		
		return view('gallery/cateditor')->with([
			'cat' => (!$categoryid ? false : $this->gallery_model->GetCategory($categoryid))
			]);
			
			
			//return $this->index();
	}
	
	public function rem_category($catid=0)
	{
		if(Auth::guest() ||  Auth::user()->roleid!=2)
                return $this->fuckyou();
			
		if ($request->get('categoryid',0)!=0)
		{
							
			$categoryid         =  $request->get('categoryid');
		
			$this->gallery_model->RmCategoryGallery($categoryid, Auth::user()->id);
		}
		
		return $this->index();
	}
	
	public function edit_album()
	{
		if(Auth::guest() || Auth::user()->roleid!=2)
                return view('aktualnosci/fucku');
            
            $albumid = false;
            $album = false;
            
            if (Input::has('albumid'))
            {
                $albumid = Input::get('albumid');
                $album = $this->gallery_model->GetAlbum($albumid);
            }
            
            //var_dump($news);
            
            return view('gallery/albumeditor')->with([
                'album' => $album
                ]);
	}
	
	public function addedit_album(\App\Http\Requests\Album $request)
	{
		if(Auth::guest() ||  Auth::user()->roleid!=2)
                return $this->fuckyou();
			
		$albumid = false;
		$news[0] = new \stdClass();
		
		//var_dump($request->get('newsid'));
		
		if ($request->get('albumid',0)!=0)
		{
							
			$albumid         =  $request->get('albumid');
			$news[0]->title     =  $request->get('title');
			$news[0]->newsid    =  $albumid;
			$news[0]->body    	=  $request->get('body');
			$news[0]->jpg    	=  $request->get('jpg');
			
			$postdata['title'] = $news[0]->title;
			$postdata['body'] = $news[0]->body;
			$postdata['jpg'] = $news[0]->jpg;
		
			$this->gallery_model->EditAlbumGallery($albumid, $postdata, Auth::user()->id);
		}
		else
		{
			$postdata['body'] = $request->get('body');
			$postdata['title'] = $request->get('title');
			$postdata['jpg'] = $request->get('jpg');
		
			$albumid = $this->gallery_model->AddAlbumGallery($postdata, Auth::user()->id);
		}
		
		return view('gallery/albumeditor')->with([
			'album' => (!$albumid ? false : $this->gallery_model->GetAlbum($albumid))
			]);
			
			//return $this->index();
	}
	
	public function rem_album($albumid=0)
	{
		if(Auth::guest() ||  Auth::user()->roleid!=2)
                return $this->fuckyou();
			
		if ($request->get('albumid',0)!=0)
		{
							
			$albumid         =  $request->get('albumid');
		
			$this->gallery_model->RmAlbumGallery($albumid, Auth::user()->id);
		}
	}
	
	public function add_photo(\App\Http\Requests\Photo $request)
	{
		if(Auth::guest() ||  Auth::user()->roleid!=2)
                return $this->fuckyou();
			
			
		$postdata['jpg'] = $request->get('jpg');
		$postdata['albumid'] = $request->get('albumid');
		
		$photoid = $this->gallery_model->AddPhotoGallery($postdata, Auth::user()->id);
	}
	
	public function rem_photo($photoid=0)
	{
		if(Auth::guest() ||  Auth::user()->roleid!=2)
                return $this->fuckyou();
			
		if ($request->get('photoid',0)!=0)
		{
							
			$photoid         =  $request->get('photoid');
		
			$this->gallery_model->RmPhotoGallery($photoid, Auth::user()->id);
		}
	}
	
	function fuckyou()
	{
		return view('aktualnosci/fucku');
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
