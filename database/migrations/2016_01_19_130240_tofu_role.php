<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TofuRole extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('tofu_role', function(Blueprint $table)
		{
		  $table->increments('id');
		 // $table -> integer('author_id') -> unsigned() -> default(0);
		  //$table->foreign('author_id')
			//  ->references('id')->on('users')
			 // ->onDelete('cascade');
		  $table->string('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('tofu_role');
	}

}
