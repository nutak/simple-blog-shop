<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TofuUser extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('tofu_user', function(Blueprint $table)
		{
		  $table->increments('id');
		 // $table -> integer('author_id') -> unsigned() -> default(0);
		  //$table->foreign('author_id')
			//  ->references('id')->on('users')
			 // ->onDelete('cascade');
		  $table->string('username');
		  $table->string('email')->unique;
		  $table->string('pass');
		  $table->integer('roleid');
		  $table->string('firstname');
		  $table->string('lastname');
		  $table->string('address');
		  $table->string('city');
		  $table->string('country');
		  $table->string('def_language');
		  $table->string('phone');
		  $table->string('postal_code');
		  //$table->text('body');
		  //$table->string('slug')->unique();
		  //$table->boolean('active');
		  //$table->timestamps();
		  $table->rememberToken();
		  $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		 Schema::drop('tofu_user');
	}

}
