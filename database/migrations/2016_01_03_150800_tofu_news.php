<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TofuNews extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		 Schema::create('tofu_news', function(Blueprint $table)
		{
		  $table->increments('id');
		  $table -> integer('author_id') -> unsigned() -> default(0);
		  $table->foreign('author_id')
			  ->references('id')->on('tofu_user')
			  ->onDelete('cascade');
		  $table->string('title')->unique();
		  $table->text('body');
		  //$table->string('slug')->unique();
		  $table->boolean('active');
		  $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('tofu_news');
	}

}
