<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TofuGalleryPhotos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		 Schema::create('tofu_gallery_photos', function(Blueprint $table)
		{
		  $table->increments('id');
		  $table -> integer('author_id') -> unsigned() -> default(0);
		  $table->foreign('author_id')
			  ->references('id')->on('tofu_user')
			  ->onDelete('cascade');
		  $table -> integer('albums_id') -> unsigned() -> default(0);
		  $table->foreign('albums_id')
			  ->references('id')->on('tofu_gallery_albums')
			  ->onDelete('cascade');
		  $table->string('title');
		  $table->string('shortname');
		  $table->string('miniature_url');
		  $table->string('photo_url');
		  $table -> integer('order')->default(0);
		  //$table->string('slug')->unique();
		  $table->boolean('hide');
		  $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('tofu_gallery_photos');
	}

}
