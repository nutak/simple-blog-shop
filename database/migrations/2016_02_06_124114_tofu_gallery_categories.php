<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TofuGalleryCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		 Schema::create('tofu_gallery_categories', function(Blueprint $table)
		{
		  $table->increments('id');
		  $table -> integer('author_id') -> unsigned() -> default(0);
		  $table->foreign('author_id')
			  ->references('id')->on('tofu_user')
			  ->onDelete('cascade');
		  $table->string('title');
		  $table->string('shortname')->unique;
		  $table -> integer('order')->default(0);
		  //$table->string('slug')->unique();
		  $table->boolean('active');
		  $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('tofu_gallery_categories');
	}

}
